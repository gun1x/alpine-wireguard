# alpine wireguard

this is a small container that can has the packages required to run wireguard in docker
o make sure to update `start.sh` with a script that start what you need. here is an example for a case your wg config can be found at `/etc/wireguard/internal.conf`:

```
#!/bin/bash
wg-quick up internal

kubernetes_wireguard_ip="{{ kubernetes_wireguard_ip }}"
internal_network_ip="{{ internal_network_ip }}"

iptables -t nat -A PREROUTING -d "${kubernetes_wireguard_ip}/32" -j DNAT --to-destination 10.100.0.1
iptables -t nat -A POSTROUTING -s 10.100.0.1/32 -j SNAT --to-source "${kubernetes_wireguard_ip}"
iptables -t nat -A POSTROUTING -d 10.100.0.1/32 -j SNAT --to-source "${internal_network_ip}"

while (true); do
  date
  wg show
  sleep 500
done
```
